package fr.ensg.sgruel.maven.plugin.apfibonnaci;


import org.apache.maven.plugin.AbstractMojo;
import fr.ensg.sgruel.maven.core.apfibonnaci.Fibonnaci;


@org.apache.maven.plugin.annotations.Mojo(name= "fibonnaci")
public class FiboMojo extends AbstractMojo {

  @org.apache.maven.plugin.annotations.Parameter(property = "rank", required = true)
  private Long rank;

  public void execute(){
    System.out.println(Fibonnaci.getValue(rank));
  }
}
