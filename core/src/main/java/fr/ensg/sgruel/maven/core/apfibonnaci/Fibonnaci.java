package fr.ensg.sgruel.maven.core.apfibonnaci;

public class Fibonnaci {
  /**
  *Return the value of the fibonnaci sequence given its rank
  *@param rank Rank to be calculated
  *@return the value of fibonnaci sequence at the given rank
  */
    public  static Long getValue(Long rank){
      if (rank <=0) return 0L;
      if (rank == 1) return 1L;
      Long value = getValue(rank-1L) + getValue(rank-2L);
      return value;

    }


}
